<?php
defined('APPLICATION_PATH') or define('APPLICATION_PATH', dirname(__FILE__));

include_once(APPLICATION_PATH . '/database/Database.php');
include_once(APPLICATION_PATH . '/classes/Render.php');
include_once(APPLICATION_PATH . '/classes/SessionManager.php');

SessionManager::start();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Assignment 4.</title>
        <link rel="stylesheet" href="css/main.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
    </head>
    <body>
        <?php
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['logout'])) {
                SessionManager::logout();
            }
        }
        ?>
        <div class="container">
            <div class="row well" id="header">
                <h1 class="col-lg-11">Animal sound questions</h1>
                 <form class="col-lg-1" role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <button class="btn btn-default" type="submit" name="logout">Log out</button>
                </form>
            </div>
            <div class="row well">
               <?php 
               // Overview achievements and when -> add date to database.
               // Hall of fame.
               // Button Do questions -> goes to the questions app.
               ?>
            </div>
        </div>
    </body>
</html>
