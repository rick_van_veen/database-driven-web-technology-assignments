<?php
defined('APPLICATION_PATH') or define('APPLICATION_PATH', dirname(__FILE__));

include_once(APPLICATION_PATH . '/../classes/SessionManager.php');
include_once(APPLICATION_PATH . '/../database/Database.php');
include_once(APPLICATION_PATH . '/../classes/Render.php');

SessionManager::start(false);

// This should be something in a static class and a singleton....
function setup_database() {
    $config = APPLICATION_PATH . '/../database/db_config.php';

    try {
        $database = new Database($config);
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
    $database->setup_connection();
    return $database;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <link rel="stylesheet" href="../css/main.css"/>
        <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    </head>
    <body>
        <div class="container">
            <div class="row well" id="header">
                <h1>Animal sound questions</h1>
            </div>
            <div class="row well">
                <h2>Log in</h2>
                <?php
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (isset($_POST['Login'])) {
                        if (!empty($_POST['Login']['username']) && !empty($_POST['Login']['password'])) {
                            $database = setup_database();
                            $user = $database->get_user($_POST['Login']['username']);
                            if ($user) {
                                if ($user['password'] == $_POST['Login']['password']) {// Hash!!
                                    SessionManager::login();
                                } else { // Wrong password
                                    Render::error_messages("Wrong password.");
                                }
                            } else { // User does not exist
                                Render::error_messages("Unknown user.");
                            }
                        } else {
                              Render::error_messages("All fields are required.");
                        }
                    }
                }
                ?>
                <form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <div class="form-group row">
                        <div class="col-lg-5">
                            <label for="username">Username</label>
                            <input type="text" name="Login[username]" id="username" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-5">
                            <label for="password">Password</label>
                            <input type="password" name="Login[password]" id="password" class="form-control"/>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-default"> Log in </button>
                    <a style="margin-left: 5px;" href="register.php">I need an account</a>
                </form>
            </div>
        </div>
    </body>
</html>