<?php
defined('APPLICATION_PATH') or define('APPLICATION_PATH', dirname(__FILE__));

include_once(APPLICATION_PATH . '/../classes/SessionManager.php');
include_once(APPLICATION_PATH . '/../database/Database.php');
include_once(APPLICATION_PATH . '/../classes/Render.php');

SessionManager::start();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Hall of fame</title>
    </head>
    <body>
        <?php
        echo 'Hall of fame';
        ?>
    </body>
</html>
