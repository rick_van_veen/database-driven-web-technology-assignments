<?php 
defined('APPLICATION_PATH') or define('APPLICATION_PATH', dirname(__FILE__));

include_once(APPLICATION_PATH . '/../database/Database.php');
include_once(APPLICATION_PATH . '/../classes/Render.php');
include_once(APPLICATION_PATH . '/../classes/SessionManager.php');

SessionManager::start(); 
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Assignment 3.</title>
    </head>
    <body>
        <?php
        /**
         * Main progam. Handles all post data and gives the appropriate response.
         */
        $config = APPLICATION_PATH . '/../database/db_config.php';

        try {
            $database = new Database($config);
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
        $database->setup_connection();

        $number_of_questions = $database->get_questions_count();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['Answer'])) { // Answer has been given
                if (isset($_POST['Answer']['answer'])) {
                    if ($database->verify_answer(SessionManager::get_value('question_nr'), $_POST['Answer']['answer'])) { // Answer given is correct
                        SessionManager::inc_variable_by('question_nr', 1);
                        SessionManager::inc_variable_by('correct_answers', 1);

                        Render::next_button(true);
                    } else {  // Answer given is not correct
                        SessionManager::inc_variable_by('question_nr', 1);
                        Render::next_button(false);
                    }
                } else {
                    SessionManager::inc_variable_by('question_nr', 1);
                    Render::next_button(false);
                }
            } else { //if (isset($_POST['submit'])) // Need to render next question
                $current_question_nr = SessionManager::get_value('question_nr');
                if ($current_question_nr <= $number_of_questions) // If next question is NULL, there are no questions left to answer
                    Render::question_form($database->get_question($current_question_nr), $database->get_choises($current_question_nr));
                else {
                    Render::finish($number_of_questions);
                    SessionManager::destroy_session();
                }
            }
        } else { // First question.
            $question_nr = SessionManager::get_value('question_nr');
            Render::question_form($database->get_question($question_nr), $database->get_choises($question_nr));
        }
        ?>
    </body>
</html>
