<?php
defined('APPLICATION_PATH') or define('APPLICATION_PATH', dirname(__FILE__));

include_once(APPLICATION_PATH . '/../classes/SessionManager.php');
include_once(APPLICATION_PATH . '/../database/Database.php');
include_once(APPLICATION_PATH . '/../classes/Render.php');

SessionManager::start(false);

// This should be something in a static class and a singleton....
function setup_database() {
    $config = APPLICATION_PATH . '/../database/db_config.php';
    try {
        $database = new Database($config);
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
    $database->setup_connection();
    return $database;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Register</title>
        <link rel="stylesheet" href="../css/main.css"/>
        <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    </head>
    <body>
        <div class="container">
            <div class="row well" id="header">
                <h1>Animal sound questions</h1>
            </div>
            <div class="row well">
                <h2>Register</h2>
                <?php
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (isset($_POST['Register'])) {
                        $username = $_POST['Register']['username'];
                        $password = $_POST['Register']['password'];
                        $password_repeat = $_POST['Register']['password_repeat'];
                    }
                    if (!empty($username)) {
                        $database = setup_database();
                        if ($database->check_availability($username)) {
                            if (!empty($password) && !empty($password_repeat)) {
                                if (strcmp($password, $password_repeat) == 0) {
                                    if ($database->save_user($username, $password)) {
                                        header("Location: http://localhost/WebTech/pages/login.php");
                                    } else {
                                        Render::error_messages("Something went wrong.");
                                    }
                                } else {
                                    Render::error_messages("Password and password repeat should match.");
                                }
                            } else {
                                Render::error_messages("All fields are required");
                            }
                        } else {
                            Render::error_messages("Username unavailable.");
                        }
                    } else {
                        Render::error_messages("All fields are required");
                    }
                }
                ?>
                <form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <div class="form-group row">
                        <div class="col-lg-5">
                            <label for="username">Username</label>
                            <input type="text" name="Register[username]" id="username" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-5">
                            <label for="password">Password</label>
                            <input type="password" name="Register[password]" id="password" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-5">
                            <label for="password">Password repeat</label>
                            <input type="password" name="Register[password_repeat]" id="password" class="form-control"/>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-default">Register</button>
                    <a style="margin-left: 5px;" href="login.php">I already got an account</a>
                </form>
            </div>
        </div>
    </body>
</html>
