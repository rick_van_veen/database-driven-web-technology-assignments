1. When using cookies the values stored in the cookies can be altered. This way 
it is possible to change the amount of correctly answered questions in a cookie. 
User could skip question by altering the post data.

2. Session data is stored on the server so it is not possible to change it on the 
client side. The cookie only contains the session id. Altering the post data to skip questions
is also not possible anymore in the same way, because the application does not 
use hidden form fields anymore to post the next question number to the server. 