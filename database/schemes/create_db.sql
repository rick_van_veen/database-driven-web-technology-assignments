CREATE DATABASE questions_db;
USE questions_db;

CREATE TABLE questions(
    q_number INT NOT NULL,
    q_text VARCHAR(128) NOT NULL,
    CONSTRAINT question_pk PRIMARY KEY (q_number)
)
ENGINE = INNODB;

CREATE TABLE choises (
    c_number INT NOT NULL,
    c_text VARCHAR(128) NOT NULL,
    correct INT NOT NULL,
    q_number INT NOT NULL,
    CONSTRAINT choise_pk PRIMARY KEY (c_number, q_number)
)
ENGINE = INNODB;

CREATE TABLE users (
    username VARCHAR(32) NOT NULL,
    password VARCHAR(128) NOT NULL,
    score INT NOT NULL DEFAULT 0,
    CONSTRAINT user_pk PRIMARY KEY (username)
)
ENGINE = INNODB;

ALTER TABLE choises ADD CONSTRAINT choise_fk FOREIGN KEY (q_number) REFERENCES questions(q_number);