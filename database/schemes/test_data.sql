USE questions_db;

INSERT INTO questions (q_number, q_text) VALUES ('1', 'What animal meows?');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('1', 'Cat', '1', '1');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('2', 'Dog', '0', '1');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('3', 'Horse', '0', '1');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('4', 'Monkey', '0', '1');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('5', 'Mouse', '0', '1');

INSERT INTO questions (q_number, q_text) VALUES ('2', 'What animal barks?');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('1', 'Cat', '0', '2');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('2', 'Dog', '1', '2');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('3', 'Horse', '0', '2');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('4', 'Monkey', '0', '2');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('5', 'Mouse', '0', '2');

INSERT INTO questions (q_number, q_text) VALUES ('3', 'What animal snorts?');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('1', 'Cat', '0', '3');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('2', 'Dog', '0', '3');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('3', 'Horse', '1', '3');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('4', 'Monkey', '0', '3');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('5', 'Mouse', '0', '3');

INSERT INTO questions (q_number, q_text) VALUES ('4', 'What animal squeaks and squeals?');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('1', 'Cat', '0', '4');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('2', 'Dog', '0', '4');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('3', 'Horse', '0', '4');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('4', 'Monkey', '0', '4');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('5', 'Mouse', '1', '4');

INSERT INTO questions (q_number, q_text) VALUES ('5', 'What animal chatters?');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('1', 'Cat', '0', '5');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('2', 'Dog', '0', '5');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('3', 'Horse', '0', '5');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('4', 'Monkey', '1', '5');
INSERT INTO choises (c_number, c_text, correct, q_number) VALUES ('5', 'Mouse', '0', '5');

INSERT INTO users (username, password) VALUES ('rick', 'rick');