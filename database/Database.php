<?php

class Database {

    /**
     * @var array contains the credentials needed for setting up a database connection. 
     */
    private $_config;

    /**
     * @var PDO object that represents the connection to the database.
     */
    private $_connection = null;

    /**
     * Constructs the database
     * @param string $config is the config file in which credentials are stored.
     */
    function __construct($config = null) {
        if ($config === null)
            throw new Exception('Database constructor: Need a configuration file');
        $this->_config = require_once($config);
    }

    /**
     * Setups the connection by creating a new PDO object. Credentials are stored in the config.
     */
    public function setup_connection() {
        try {
            $this->_connection = new PDO($this->_config['dsn'], $this->_config['username'], $this->_config['password']);
        } catch (PDOException $e) {
            echo 'Database connection failed: ' . $e->getMessage();
        }
    }

    /**
     * Will get the amount of questions stored in the database
     * @param string $table_name the name of the table.
     * @return PDO object on succes and FALSE on failure
     */
    public function get_questions_count() {
        if ($this->_connection === null)
            $this->setup_connection();

        $count = 0;
        $result = $this->_connection->query('SELECT * FROM questions');

        if ($result != false)
            $count = $result->rowCount();

        return $count;
    }

    /**
     * Will get the question speciefied by $question_number
     * @param int $question_number the number we want.
     * @return question array on succes and FALSE on failure
     */
    public function get_question($question_number) {
        if ($this->_connection === null)
            $this->setup_connection();

        $sql_string = 'SELECT * FROM questions WHERE q_number = ?';
        $sql_query = $this->_connection->prepare($sql_string);

        $question = false;
        if ($sql_query->execute(array($question_number)))
            $question = $sql_query->fetch();

        return $question;
    }

    /**
     * Gets all the choises belonging to the question speciefied by $question_number
     * @param int $question_number
     * @return PDO object on succes and FALSE on failure.
     */
    public function get_choises($question_number) {
        if ($this->_connection === null)
            $this->setup_connection();

        $sql_string = 'SELECT * FROM choises WHERE q_number = ?';
        $sql_query = $this->_connection->prepare($sql_string);

        $choises = false;
        if ($sql_query->execute(array($question_number)))
            $choises = $sql_query;

        return $choises;
    }

    /**
     * Gets the correct answere belonging to the question specified by $question_number.
     * @param int $question_number the number of the question.
     * @return an array containing the correct c_number, FALSE otherwise
     */
    private function get_correct_answer($question_number) {
        if ($this->_connection === null)
            $this->setup_connection();

        $sql_string = 'SELECT c_number FROM choises WHERE q_number = ? AND correct = 1';
        $sql_query = $this->_connection->prepare($sql_string);

        $correct_choise = false;
        if ($sql_query->execute(array($question_number)))
            $correct_choise = $sql_query->fetch();

        return $correct_choise;
    }

    /**
     * Verifies the answer given to a certain question.
     * @param int $question_number the number of the question.
     * @param int $answer_number the number of the answer. 
     * @return TRUE if the ansdwer is correct and FALSE otherwise
     */
    public function verify_answer($question_number, $answer_number) {
        $correct_choise = $this->get_correct_answer($question_number);
        return $correct_choise['c_number'] == "$answer_number" ? true : false;
    }

    public function get_user($username) {
        if ($this->_connection === null)
            $this->setup_connection();

        $sql_string = 'SELECT * FROM users WHERE username = ?';
        $sql_query = $this->_connection->prepare($sql_string);

        $user = false;
        if ($sql_query->execute(array($username)))
            $user = $sql_query->fetch();

        return $user;
    }

    public function check_availability($username) {
        if ($this->_connection === null)
            $this->setup_connection();

        $sql_string = 'SELECT username FROM users WHERE username = ?';
        $sql_query = $this->_connection->prepare($sql_string);

        $user = true;
        if ($sql_query->execute(array($username)))
            $user = $sql_query->fetch();

        return !$user ? true : false;
    }

    public function save_user($username, $password) {
        if ($this->_connection === null)
            $this->setup_connection();

        $sql_string = 'INSERT INTO users (username, password) VALUES (?, ?);';
        $sql_query = $this->_connection->prepare($sql_string);

        return $sql_query->execute(array($username, $password));
    }
    public function get_score($username) {
        if ($this->_connection === null)
            $this->setup_connection();
        
        $sql_string = 'SELECT score FROM users WHERE username = ?';
        $sql_query = $this->_connection->prepare($sql_string);
        
        $score = false;
        if ($sql_query->execute(array($username)))
            $score = $sql_query->fetch();

        return $score;
    }
}
