<?php

class SessionManager {

    private static $_lifetime = 604800;
    private static $_session_name = 'Questions';

    /**
     * Set the session variables
     */
    private static function set_session_params() {
        //session_set_cookie_params(self::$_lifetime);
        session_name(self::$_session_name);
    }

    /**
     * Calls set_session_params and starts session. If it is a new session it wil initialize the session variables.
     */
    public static function start($private = true) {
        self::set_session_params();
        session_start();
        var_dump(session_id());
        var_dump($_SESSION);
        if (!self::exists('expiration_time')) { // New session.
            self::init();
            self::start();
        } else { // Session is resumed.
            if (time() > self::get_value('expiration_time')) {
                self::destroy_session();
                self::start();
            } else { // Valid session
                if ($private && !self::get_value('logged_in')) { // Private page and not logged in -> redirect to login.php
                    header("Location: http://localhost/WebTech/pages/login.php");
                }
            }
        }
    }

    public static function login() {
        self::set_variable('logged_in', 1);
        header("Location: http://localhost/WebTech/index.php");
    }
    
    public static function logout() {
        self::set_variable('logged_in', 0);
        header("Location: http://localhost/WebTech/pages/login.php");
    }

    /**
     * Initialized the session variables
     */
    public static function init() {
        $expiration_time = time() + self::$_lifetime;
        // Set session variable and cookie to the same expiration_time
        self::create_variable('expiration_time', $expiration_time);
        setcookie(self::$_session_name, session_id(), $expiration_time, '/');

        // Init the other session variables.
        self::create_variable('logged_in', 0);
        self::create_variable('question_nr', 1);
        self::create_variable('correct_answers', 0);
    }

    /**
     * Looks if a variable called $variable exists.
     * @param string $variable name of the variable
     * @return boolean TRUE if the $variable exists FALSE otherwise
     */
    public static function exists($variable) {
        return isset($_SESSION[$variable]);
    }
    
    public static function set_variable($variable, $value) {
        if(self::exists($variable))
            $_SESSION[$variable] = $value;
    }

    /**
     * Creates a session variable if it doesnt already exist
     * @param string $variable the name of the variable
     * @param mixed $value the initialization value for the variable
     */
    public static function create_variable($variable, $value) {
        if (!self::exists($variable))
            $_SESSION[$variable] = $value;
    }

    /**
     * Will increment the session $variable with $value if it exists.
     * @param string $variable name of the variable that needs to be incremented
     * @param int $value is the value with what the $variable should be incremented.
     * @return boolean on failure FALSE
     */
    public static function inc_variable_by($variable, $value) {
        if (self::exists($variable))
            $_SESSION[$variable] += $value;
        else
            return false;
    }

    /**
     * Will get the value of the session variable named variable
     * @param string $variable the name of the variable
     * @return mixed the value of the variable $variable or false if it does not exist.
     */
    public static function get_value($variable) {
        return self::exists($variable) ? $_SESSION[$variable] : false;
    }

    /**
     * Destroys the session
     */
    public static function destroy_session() {
        session_destroy();
    }

    /**
     * Debugging function @todo remove
     */
    public static function print_session() {
        echo 'id = ' . session_id() . ', name = ' . session_name() . ', values = question_nr => ' . self::get_value('question_nr') . ', correct_answers => ' . self::get_value('correct_answers') . '.';
    }

}