<?php

class Render {

    /**
     * 	Function echos a form given a question, with its possible_answers
     */
    public static function question_form($question, $choises) {
        echo '<h1> Question #' . $question['q_number'] . ' ' . $question['q_text'] . '</h1>';

        echo '<form action = "index.php", method="post">';
        foreach ($choises as $choise) {
            echo '<input type="radio" name="Answer[answer]" value="' . $choise['c_number'] . '" id="choise_' . $question['q_number'] . $choise['c_number'] . '">';
            echo '<label for="choise_' . $question['q_number'] . $choise['c_number'] . '">' . $choise['c_text'] . '</label> </input><br/>';
        }
        echo '<input type="submit" value="Submit">';
        echo '</form>';
    }

    public static function next_button($correct) {
        if ($correct)
            self::correct_response();
        else
            self::incorrect_response();

        echo '<form action="index.php" method="post">';
        echo '<input type = "submit" name="submit" value = "Next">';
        echo '</form>';
    }

    /**
     * Renders the response when the given answer is correct. Also renders the next button.
     */
    public static function correct_response() {
        echo '<h1>Your answer is correct!</h1>' . "\n";
    }

    /**
     * Renders the response when the given answer is not correct. Also renders the retry button.
     */
    public static function incorrect_response() {
        echo '<h1>Your answer is not correct!</h1>' . "\n";
    }

    /**
     * Renders the final response
     */
    public static function finish($total) {
        echo '<h1> You have answered all questions </h1>' . "\n";
        echo '<p> You have answered ' . SessionManager::get_value('correct_answers') . ' out of ' . $total . ' questions correctly </p>' . "\n";
    }
    
    public static function success_messages($messages) {
        echo '<div class="alert alert-success">' . $messages . '</div>' . "\n";
    }
    
    public static function error_messages($messages) {
        echo '<div class="alert alert-danger">' . $messages . '</div>' . "\n";
    }
}
